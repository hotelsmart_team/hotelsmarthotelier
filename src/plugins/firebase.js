import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/database';
import 'firebase/storage'
import 'firebase/analytics'
// import { bus } from '@/plugins/bus'

let config = {
  apiKey: "AIzaSyBuQnAPb7bCOFYYB2zmreWK1w2giINl0Ps",
  authDomain: "hotelsmart-a28dd.firebaseapp.com",
  databaseURL: "https://hotelsmart-a28dd.firebaseio.com",
  projectId: "hotelsmart-a28dd",
  appId: "1:794950000481:web:3e1821138f9ca7c173af20",
  messagingSenderId: '794950000481',
  storageBucket: 'hotelsmart-a28dd.appspot.com',
  measurementId: "G-FWG8FN41LT"
};

// const firebase = firebase


const app = !firebase.apps.length ? firebase.initializeApp(config) : ''
firebase.analytics();
const db = firebase.firestore();
const database = firebase.database();
// const messaging = firebase.messaging();
const storage = firebase.storage()


firebase.firestore().enablePersistence({synchronizeTabs: true})
.catch(function(err) {
  if (err.code == 'failed-precondition') {
    // Multiple tabs open, persistence can only be enabled
    // in one tab at a a time.
    // ...
      // console.log(err)
  } else if (err.code == 'unimplemented') {
    // The current browser does not support all of the
    // features required to enable persistence
    // ...
    // console.log(err)
  }
});

// const VAPIDKEY = "BIAMuzlwY89qC2tt4FP3GdyH4cWG-2ZjUQyUKjgCb_DAdh1eKYrWf2A6cwGmKYj-jizz4iobRGnEvwhOQq3WpHM"
// messaging.usePublicVapidKey(VAPIDKEY)

// messaging.onMessage(payload => {
//   // console.log(payload.notification)

//   bus.$emit('Snackbar', {
//     message: payload.notification.body,
//     show: true,
//     color: 'info'
//   })
// })

export { firebase, db, database, app, storage }