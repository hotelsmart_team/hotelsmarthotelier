const JavaScriptObfuscator = require('javascript-obfuscator');
const fs = require('fs')
const path = require('path')
const folder = path.resolve(__dirname, 'dist/js')
const options = {
  compact: true,
  controlFlowFlattening: true,
  controlFlowFlatteningThreshold: 0.75,
  deadCodeInjection: true,
  deadCodeInjectionThreshold: 0.4,
  debugProtection: false,
  domainLock: ['.hotelsmart.com.ng'],
  debugProtectionInterval: false,
  disableConsoleOutput: true,
  identifierNamesGenerator: 'hexadecimal',
  log: false,
  renameGlobals: false,
  rotateStringArray: true,
  selfDefending: true,
  shuffleStringArray: true,
  splitStrings: true,
  splitStringsChunkLength: 10,
  stringArray: true,
  stringArrayEncoding: 'base64',
  stringArrayThreshold: 0.75,
  transformObjectKeys: true,
  unicodeEscapeSequence: false
}

fs.readdir(folder, (err, files) => {
  // console.log(files)
  files.filter(f => f.endsWith('.js')).forEach(file => {
    
    var filePath = path.resolve(__dirname, 'dist/js', file)
    fs.readFile(filePath, {encoding: 'utf8'}, (err, data) => {
      
      var obfuscationResult = JavaScriptObfuscator.obfuscate(data, options)
      var transformed = obfuscationResult.getObfuscatedCode()
      fs.writeFile(filePath, transformed, (err) => {
        if (err) throw err;
        console.log('complete')
      })
    })
  })
})