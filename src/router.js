import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from './views/Dashboard.vue'
import {firebase} from './plugins/firebase'

Vue.use(Router)


const requireAuth = (to, from, next) => {
  firebase.auth().onAuthStateChanged((user)=>{
    if(user){
      
      next()
    }
    else{
      next('/login')
    }
  });
  
}
const requireLogout = (to, from, next) => {
  firebase.auth().onAuthStateChanged((user)=>{
    if(user){
      
      next('/')
    }
    else{
      next()
    }
  });
  
}

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: Dashboard,
      beforeEnter: requireAuth,
      children: [
        {
          path: '',
          name: 'home',
          component: () => import('@/components/Home.vue')
        },
        {
          path: '/reservations',
          name: 'reservations',
          component: () => import('@/components/reservations.vue')
        },
        // {
        //   path: '/reservations/:bookingId',
        //   name: 'view_reservations',
        //   component: () => import('@/components/viewReservation.vue')
        // },
        {
          path: '/rates-availability-inventory',
          name: '',
          component: () => import('@/components/rates-availability-inventory/index.vue'),
          children: [
            
            {
              path: '',
              name: 'calendar',
              component: () => import('@/components/rates-availability-inventory/calendar.vue')
            },
            {
              path: 'rooms',
              name: 'rooms',
              component: () => import('@/components/rates-availability-inventory/rooms.vue')
            },
          ]
        },
        {
          path: '/messages/inbox',
          name: 'inbox',
          component: () => import('@/components/guests/messages/inbox')
        },
        {
          path: '/messages/sent',
          name: 'sent',
          component: () => import('@/components/guests/messages/sent.vue')
        },
        {
          path: '/messages/open/:docId',
          name: 'open',
          component: () => import('@/components/guests/messages/open.vue')
        },
        {
          path: '/guests-management',
          name: 'guests-management',
          component: () => import('@/components/guests/guests-management')
        },
        {
          path: '/guests-management/:guestId',
          name: 'guest_profile',
          component: () => import('@/components/guests/guest-profile')
        },
        {
          path: '/orders',
          name: 'orders',
          component: () => import('@/components/hotel-management/orders.vue')
        },
        {
          path: '/house-keeping',
          name: 'house-keeping',
          component: () => import('@/components/hotel-management/house-keeping.vue')
        },
        {
          path: '/shift-manager',
          name: 'shift-manager',
          component: () => import('@/components/hotel-management/shift-manager.vue')
        },
        {
          path: '/shop-manager',
          name: 'shop-manager',
          component: () => import('@/components/hotel-management/shop-manager.vue')
        },
        {
          path: '/reports',
          name: 'reports',
          component: () => import('@/components/reports.vue')
        },
        // {
        //   path: '/payments',
        //   name: 'payments',
        //   component: () => import('@/components/payments.vue')
        // },
        {
          path: '/setup',
          component: () => import('@/components/setup/index.vue'),
          children: [
            {
              path: '',
              name: 'general',
              component: () => import('@/components/setup/general.vue')
            },
            {
              path: 'media',
              name: 'media',
              component: () => import('@/components/setup/media.vue')
            },
            {
              path: 'faq',
              name: 'faq',
              component: () => import('@/components/setup/faq.vue')
            },
            // {
            //   path: 'accounts',
            //   name: 'accounts',
            //   component: () => import('@/components/setup/accounts.vue')
            // },
            {
              path: 'activate',
              name: 'activate',
              component: () => import('@/components/setup/activate.vue')
            },
            {
              path: 'notifications',
              name: 'notifications',
              component: () => import('@/components/setup/notifications.vue')
            },
            {
              path: 'reservations',
              name: 'reservations_settings',
              component: () => import('@/components/setup/reservations.vue')
            },
            {
              path: 'users-and-permissions',
              name: 'users-and-permissions',
              component: () => import('@/components/setup/users-and-permissions.vue')
            },
          ]
        },
        {
          path: '/profile',
          name: 'profile',
          component: () => import('@/components/profile.vue')
        },
        {
          path: '/404',
          name: '404',
          component: () => import('./views/404.vue')
        }
      ]
    },
    {
      path: '/login',
      name: 'login',
      component: () => import(/* webpackChunkName: "about" */ './views/Login.vue'),
      beforeEnter: requireLogout
    },
    {
      path: '/404',
      name: '404_',
      component: () => import('./views/404.vue')
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (!to.matched.length) {
    next('/404');
  } else {
    next();
  }
});

export default router
