// webpack.config.js
const JavaScriptObfuscator = require('webpack-obfuscator');
module.exports = {
  rules: [
    {
      test: /\.s(c|a)ss$/,
      use: [
        'vue-style-loader',
        'css-loader',
        {
          loader: 'sass-loader',
          // Requires sass-loader@^7.0.0
          options: {
            implementation: require('sass'),
            fiber: require('fibers'),
            indentedSyntax: true // optional
          }
        }
      ]
    },
    {
      test: /\.vue$/,
      include: [ path.resolve(__dirname, "src/components") ],
      enforce: 'post',
      use: { loader: 'obfuscator-loader', options: {
        rotateUnicodeArray: true,
        debugProtection: true,
        debugProtectionInterval: false,
        disableConsoleOutput: true,
        domainLock: ['.hotelsmart.com.ng'],
        seed: 10,
        selfDefending: true,
        transformObjectKeys: true
      }}
    },
  ],
  // plugins: [
  //   new JavaScriptObfuscator({
  //     rotateUnicodeArray: true,
  //     debugProtection: true,
  //     debugProtectionInterval: false,
  //     disableConsoleOutput: true,
  //     domainLock: ['.hotelsmart.com.ng'],
  //     seed: 10,
  //     selfDefending: true,
  //     transformObjectKeys: true
  //   })
  // ]
}